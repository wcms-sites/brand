core = 7.x
api = 2

; views_datasource
projects[views_datasource][type] = "module"
projects[views_datasource][download][type] = "git"
projects[views_datasource][download][url] = "https://git.uwaterloo.ca/drupal-org/views_datasource.git"
projects[views_datasource][download][tag] = "7.x-1.0-alpha2"
projects[views_datasource][subdir] = ""

; uw_ct_logos
projects[uw_ct_logos][type] = "module"
projects[uw_ct_logos][download][type] = "git"
projects[uw_ct_logos][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_logos.git"
projects[uw_ct_logos][download][tag] = "7.x-1.4"
projects[uw_ct_logos][subdir] = ""
